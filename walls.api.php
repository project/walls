<?php

/**
 * @file
 * Hooks provided by the Walls module.
 */

/**
 * Perform alterations on wall-loaded.
 */
function hook_walls_wall_alter(&$wall) {
  $wall->text = 'Share something...';
}

/**
 * Implement this hook in your module to define new walls-status attachment types.
 * @see walls_walls_post_attachment().
 * @return array of attachment-elements information.
 */
function hook_walls_post_attachment() {
  $attachments = array();
  return $attachments;
}
